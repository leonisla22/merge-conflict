# Workshop: Merge Conflicts

## Equipo: Dream Team

## Pasos 

### 1 - Clonar repositorio 

`git clone https://gitlab.com/leonisla22/merge-conflict.git`

### 2 - Navegar a la carpeta

`cd merge-conflict`

### 3 - Pararse en la rama master

`git checkout master`

### 4 - Hacer el merge!

Con el branch tal cual en el remote:
- `git merge origin/feature2`

O si tienen el branch de forma local:
- `git merge feature2`


Output:
```
Auto-merging fileA.txt
CONFLICT (content): Merge conflict in fileA.txt
Automatic merge failed; fix conflicts and then commit the result.
```

### 5 - Ver el conflicto

`git diff`

Output:
```
diff --cc fileA.txt
index 53bcd31,94b5d4d..0000000
--- a/fileA.txt
+++ b/fileA.txt
@@@ -1,1 -1,1 +1,5 @@@
++<<<<<<< HEAD
 +feature3
++=======
+ feature2
++>>>>>>> feature2
```

### 6 - Resolver editandolo con tu editor favorito

- `code fileA.txt`
- `nano fileA.txt`

### 7 - Agregar el archivo una vez resulto el conflicto

`git add fileA.txt`

### 8 - Commit!

Con mensaje definido por nosotros

- `git commit -m "Merge branch 'feature2'"`

O el automático generado por git

- `git commit --no-edit`

## Extras

### Deshacer y volver a intentar:

Parados desde la rama master:

`git reset --hard origin/master`

Nota: van a perder todos los cambios realizados
